@extends('partial.index')
@section('content')
    <style>
        .fit {
            height: 500px;
        }

        .content {
            position: relative;
            display: inline-block;
        }

        .overlay {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.945);
            /* Warna hitam dengan tingkat transparansi 0.5 */
            opacity: 0.5;
            /* Atur tingkat transparansi sesuai kebutuhan Anda */
        }
    </style>

    <body>

        <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active"
                    aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"
                    aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"
                    aria-label="Slide 3"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="3"
                    aria-label="Slide 4"></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active content">
                    <img src="{{ asset('assets/img/baner/2.png') }}" class="d-block w-100 fit " alt="...">
                    <div class="overlay"></div>
                    <div class="carousel-caption ">
                        <h2>Anak-Anak Pantas Mendapatkan Kesempatan Untuk MELIHAT Keindahan Dunia Di Sekitar Mereka</h2>
                        <button type="button" class="btn btn-danger">Donasi</button>
                    </div>
                </div>
                <div class="carousel-item content">
                    <img src="{{ asset('assets/img/baner/6.png') }}" class="d-block w-100 fit" alt="...">
                    <div class="overlay"></div>
                    <div class="carousel-caption ">
                        <h2>Mereka Buta Karena Mereka Miskin, Dan Mereka Miskin Karena Mereka Buta</h2>
                        <p>Bantu Mereka Untuk Memutus Siklus Kemiskinan Dan Kebutaan Ini</p>
                        <button type="button" class="btn btn-danger">Donasi</button>
                    </div>
                </div>
                <div class="carousel-item content">
                    <img src="{{ asset('assets/img/baner/1.png') }}" class="d-block w-100 fit" alt="...">
                    <div class="overlay"></div>
                    <div class="carousel-caption ">
                        <h2>Bantuan Tajam Penglihatan Dapat Menghasilkan Dampak Yang Mengubah Hidup</h2>
                        <p>Kontribusi Kecil Dalam Pencapaian Yang Luar Biasa</p>
                        <button type="button" class="btn btn-danger">Donasi</button>
                    </div>
                </div>
                <div class="carousel-item content">
                    <img src="{{ asset('assets/img/baner/4.png') }}" class="d-block w-100 fit" alt="...">
                    <div class="overlay"></div>
                    <div class="carousel-caption ">
                        <h2>Everyone Can See Better</h2>
                        <button type="button" class="btn btn-danger">Donasi</button>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions"
                data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions"
                data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>

        <section id="about" class="about">
            <div class="container">
                <div class="section-title">
                    <span>Tentang Kami</span>
                    <h2>Tentang Kami</h2>

                </div>
                <div class="row">
                    <div class="col-lg-6 order-1 order-lg-2" data-aos="fade-left">
                        <img src="{{ asset('assets/img/baner/keg4.png') }}" class="img-fluid" alt="">
                    </div>
                    <div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content" data-aos="fade-right">
                        <h3>Indonesia Melihat</h3>
                        <p style="text-align: justify;">Selamat datang di Indonesia Melihat, tempat di mana harapan bertemu
                            dengan penglihatan dan cinta
                            membuka jalan bagi cahaya. Kami adalah sebuah tim berdedikasi yang bergerak di bidang
                            kemanusiaan, bertekad untuk mengatasi tantangan kelainan refraksi dan pencegahan kebutaan di
                            Indonesia dan memberikan dampak yang
                            nyata bagi masyarakat.</p>

                        <p style="text-align: justify;">
                            Kami tidak hanya berfokus pada perawatan mata, tetapi juga pada memberikan harapan kepada
                            individu dan keluarga yang terkena dampak dalam masalah penglihatan. Kami berusaha untuk menjadi
                            komunitas yang membawa perubahan
                            dalam perjuangan melawan kegelapan, dengan membuka pintu menuju penglihatan yang lebih baik,
                            kebebasan, dan kemandirian.
                        </p>
                    </div>
                </div>

            </div>
        </section>




        <!-- End Why Us Section -->



        <!-- ======= Services Section ======= -->
        <section id="pricing" class="pricing">
            <div class="container">

                <div class="section-title">
                    <span>Event</span>
                    <h2>Event</h2>
                    <p>Event-Event</p>
                </div>

                <div class="row">

                    <div class="col-lg-4 col-md-6 mt-4 mt-md-0" data-aos="zoom-in" data-aos-delay="150">
                        <div class="card">
                            <div class="card-body">
                                <div class="box">
                                    <h4>OKULI </h4>
                                    <ul>
                                        <li>2023</li>
                                        <li>Bandung 4 November 2023</li>
                                        <p>Optometri Komunitas Universal Literasi Indonesia</p>
                                        <p>Sosialisasi Pengabdian Kepada Masyarakat & Didactic Course Optometri Komuitas</p>

                                    </ul>
                                    <div class="btn-wrap">
                                        <a href="/okuli" class="btn-buy">Cek</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>

            </div>
        </section><!-- End Pricing Section -->

        {{-- <section id="services" class="services">
            <div class="container">

                <div class="section-title">
                    <span>Kegiatan</span>
                    <h2>Kegiatan</h2>
                    <p>Kegiatan Kegiatan</p>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="fade-up">
                        <div class="icon-box">
                            <div class="icon"><i class="bx bxl-dribbble"></i></div>
                            <h4><a href="">Lorem Ipsum</a></h4>
                            <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi
                            </p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="fade-up"
                        data-aos-delay="150">
                        <div class="icon-box">
                            <div class="icon"><i class="bx bx-file"></i></div>
                            <h4><a href="">Sed ut perspiciatis</a></h4>
                            <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
                            </p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0" data-aos="fade-up"
                        data-aos-delay="300">
                        <div class="icon-box">
                            <div class="icon"><i class="bx bx-tachometer"></i></div>
                            <h4><a href="">Magni Dolores</a></h4>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="fade-up"
                        data-aos-delay="450">
                        <div class="icon-box">
                            <div class="icon"><i class="bx bx-world"></i></div>
                            <h4><a href="">Nemo Enim</a></h4>
                            <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="fade-up"
                        data-aos-delay="600">
                        <div class="icon-box">
                            <div class="icon"><i class="bx bx-slideshow"></i></div>
                            <h4><a href="">Dele cardo</a></h4>
                            <p>Quis consequatur saepe eligendi voluptatem consequatur dolor consequuntur</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="fade-up"
                        data-aos-delay="750">
                        <div class="icon-box">
                            <div class="icon"><i class="bx bx-arch"></i></div>
                            <h4><a href="">Divera don</a></h4>
                            <p>Modi nostrum vel laborum. Porro fugit error sit minus sapiente sit aspernatur
                            </p>
                        </div>
                    </div>

                </div>

            </div>
        </section> --}}

        <!-- End Services Section -->

        <!-- ======= Cta Section ======= -->
        <section id="cta" class="cta">
            <div class="container" data-aos="zoom-in">

                <div class="text-center">
                    <h3>Selamat datang di Indonesia Melihat | Membuka Cahaya, Menebar Harapan!</h3>
                    <p> Kami berkomitmen kuat terhadap kemanusiaan dalam masyarakat. Sebagai
                        pionir dalam upaya membantu penglihatan dan mencegah kebutaan diIndonesia, Indonesia
                        Melihat telah
                        memberikan
                        cahaya kepada ribuan orang di seluruh penjuru negeri.</p>
                    <a class="cta-btn" href="#">Donasi</a>
                </div>

            </div>
        </section><!-- End Cta Section -->

        <!-- ======= Portfolio Section ======= -->

        <section id="portfolio" class="portfolio">
            <div class="container">

                <div class="section-title">
                    <span>Galeri</span>
                    <h2>Galeri</h2>
                    <p>Momen Momen Indah Kami</p>
                </div>



                <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="150">



                    <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                        <img src="{{ asset('assets/img/baner/keg1.png') }}" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Kegiatan</h4>
                            <p>Bakti Sosial</p>
                            <a href="{{ asset('assets/img/baner/keg1.png') }}" data-gallery="portfolioGallery"
                                class="portfolio-lightbox preview-link" title="Kegiatan"><i class="bx bx-plus"></i></a>
                            <a href="portfolio-details.html" class="details-link" title="More Details"><i
                                    class="bx bx-link"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                        <img src="{{ asset('assets/img/baner/keg2.png') }}" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Kegiatan</h4>
                            <p>Bakti Sosial</p>
                            <a href="{{ asset('assets/img/baner/keg2.png') }}" data-gallery="portfolioGallery"
                                class="portfolio-lightbox preview-link" title="Kegiatan"><i class="bx bx-plus"></i></a>
                            <a href="portfolio-details.html" class="details-link" title="More Details"><i
                                    class="bx bx-link"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                        <img src="{{ asset('assets/img/baner/keg3.png') }}" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Kegiatan</h4>
                            <p>Bakti Sosial</p>
                            <a href="{{ asset('assets/img/baner/keg3.png') }}" data-gallery="portfolioGallery"
                                class="portfolio-lightbox preview-link" title="Kegiatan"><i class="bx bx-plus"></i></a>
                            <a href="portfolio-details.html" class="details-link" title="More Details"><i
                                    class="bx bx-link"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                        <img src="{{ asset('assets/img/baner/keg6.png') }}" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Kegiatan</h4>
                            <p>Bakti Sosial</p>
                            <a href="{{ asset('assets/img/baner/keg6.png') }}" data-gallery="portfolioGallery"
                                class="portfolio-lightbox preview-link" title="Kegiatan"><i class="bx bx-plus"></i></a>
                            <a href="portfolio-details.html" class="details-link" title="More Details"><i
                                    class="bx bx-link"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                        <img src="{{ asset('assets/img/baner/keg7.png') }}" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Kegiatan</h4>
                            <p>Bakti Sosial</p>
                            <a href="{{ asset('assets/img/baner/keg7.png') }}" data-gallery="portfolioGallery"
                                class="portfolio-lightbox preview-link" title="Kegiatan"><i class="bx bx-plus"></i></a>
                            <a href="portfolio-details.html" class="details-link" title="More Details"><i
                                    class="bx bx-link"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                        <img src="{{ asset('assets/img/baner/keg8.png') }}" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Kegiatan</h4>
                            <p>Bakti Sosial</p>
                            <a href="{{ asset('assets/img/baner/keg8.png') }}" data-gallery="portfolioGallery"
                                class="portfolio-lightbox preview-link" title="Kegiatan"><i class="bx bx-plus"></i></a>
                            <a href="portfolio-details.html" class="details-link" title="More Details"><i
                                    class="bx bx-link"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                        <img src="{{ asset('assets/img/baner/start1.png') }}" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Kegiatan</h4>
                            <p>Bakti Sosial</p>
                            <a href="{{ asset('assets/img/baner/start1.png') }}" data-gallery="portfolioGallery"
                                class="portfolio-lightbox preview-link" title="Kegiatan"><i class="bx bx-plus"></i></a>
                            <a href="portfolio-details.html" class="details-link" title="More Details"><i
                                    class="bx bx-link"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                        <img src="{{ asset('assets/img/baner/start2.png') }}" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Kegiatan</h4>
                            <p>Bakti Sosial</p>
                            <a href="{{ asset('assets/img/baner/start2.png') }}" data-gallery="portfolioGallery"
                                class="portfolio-lightbox preview-link" title="Kegiatan"><i class="bx bx-plus"></i></a>
                            <a href="portfolio-details.html" class="details-link" title="More Details"><i
                                    class="bx bx-link"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                        <img src="{{ asset('assets/img/baner/start3.png') }}" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Kegiatan</h4>
                            <p>Bakti Sosial</p>
                            <a href="{{ asset('assets/img/baner/start3.png') }}" data-gallery="portfolioGallery"
                                class="portfolio-lightbox preview-link" title="Kegiatan"><i class="bx bx-plus"></i></a>
                            <a href="portfolio-details.html" class="details-link" title="More Details"><i
                                    class="bx bx-link"></i></a>
                        </div>
                    </div>



                </div>

            </div>
        </section>

        <!-- End Portfolio Section -->

        <!-- ======= Pricing Section ======= -->

        {{-- <section id="pricing" class="pricing">
            <div class="container">

                <div class="section-title">
                    <span>Pricing</span>
                    <h2>Pricing</h2>
                    <p>Sit sint consectetur velit quisquam cupiditate impedit suscipit alias</p>
                </div>

                <div class="row">

                    <div class="col-lg-4 col-md-6" data-aos="zoom-in" data-aos-delay="150">
                        <div class="box">
                            <h3>Free</h3>
                            <h4><sup>$</sup>0<span> / month</span></h4>
                            <ul>
                                <li>Aida dere</li>
                                <li>Nec feugiat nisl</li>
                                <li>Nulla at volutpat dola</li>
                                <li class="na">Pharetra massa</li>
                                <li class="na">Massa ultricies mi</li>
                            </ul>
                            <div class="btn-wrap">
                                <a href="#" class="btn-buy">Buy Now</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 mt-4 mt-md-0" data-aos="zoom-in">
                        <div class="box featured">
                            <h3>Business</h3>
                            <h4><sup>$</sup>19<span> / month</span></h4>
                            <ul>
                                <li>Aida dere</li>
                                <li>Nec feugiat nisl</li>
                                <li>Nulla at volutpat dola</li>
                                <li>Pharetra massa</li>
                                <li class="na">Massa ultricies mi</li>
                            </ul>
                            <div class="btn-wrap">
                                <a href="#" class="btn-buy">Buy Now</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 mt-4 mt-lg-0" data-aos="zoom-in" data-aos-delay="150">
                        <div class="box">
                            <h3>Developer</h3>
                            <h4><sup>$</sup>29<span> / month</span></h4>
                            <ul>
                                <li>Aida dere</li>
                                <li>Nec feugiat nisl</li>
                                <li>Nulla at volutpat dola</li>
                                <li>Pharetra massa</li>
                                <li>Massa ultricies mi</li>
                            </ul>
                            <div class="btn-wrap">
                                <a href="#" class="btn-buy">Buy Now</a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </section> --}}

        <!-- End Pricing Section -->

        <!-- ======= Team Section ======= -->
        {{-- <section id="team" class="team">
            <div class="container">

                <div class="section-title">
                    <span>Team</span>
                    <h2>Team</h2>
                    <p>Orang Orang Hebat Bersama Kami</p>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in">
                        <div class="member">
                            <img src="assets/img/team/team-1.jpg" alt="">
                            <h4>Walter White</h4>
                            <span>Chief Executive Officer</span>
                            <p>
                                Magni qui quod omnis unde et eos fuga et exercitationem. Odio veritatis
                                perspiciatis quaerat
                                qui aut aut aut
                            </p>
                            <div class="social">
                                <a href=""><i class="bi bi-twitter"></i></a>
                                <a href=""><i class="bi bi-facebook"></i></a>
                                <a href=""><i class="bi bi-instagram"></i></a>
                                <a href=""><i class="bi bi-linkedin"></i></a>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in">
                        <div class="member">
                            <img src="assets/img/team/team-2.jpg" alt="">
                            <h4>Sarah Jhinson</h4>
                            <span>Product Manager</span>
                            <p>
                                Repellat fugiat adipisci nemo illum nesciunt voluptas repellendus. In
                                architecto rerum rerum
                                temporibus
                            </p>
                            <div class="social">
                                <a href=""><i class="bi bi-twitter"></i></a>
                                <a href=""><i class="bi bi-facebook"></i></a>
                                <a href=""><i class="bi bi-instagram"></i></a>
                                <a href=""><i class="bi bi-linkedin"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in">
                        <div class="member">
                            <img src="assets/img/team/team-3.jpg" alt="">
                            <h4>William Anderson</h4>
                            <span>CTO</span>
                            <p>
                                Voluptas necessitatibus occaecati quia. Earum totam consequuntur qui porro
                                et laborum toro
                                des clara
                            </p>
                            <div class="social">
                                <a href=""><i class="bi bi-twitter"></i></a>
                                <a href=""><i class="bi bi-facebook"></i></a>
                                <a href=""><i class="bi bi-instagram"></i></a>
                                <a href=""><i class="bi bi-linkedin"></i></a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </section> --}}

        <!-- End Team Section -->

        <!-- ======= Clients Section ======= -->
        {{-- <section id="clients" class="clients">
            <div class="container" data-aos="zoom-in">
                <div class="section-title">
                    <span>Partner</span>
                    <h2>Partner</h2>

                </div>
                <div class="row d-flex align-items-center">

                    <div class="col-lg-2 col-md-4 col-6">
                        <img src="assets/img/clients/client-1.png" class="img-fluid" alt="">
                    </div>

                    <div class="col-lg-2 col-md-4 col-6">
                        <img src="assets/img/clients/client-2.png" class="img-fluid" alt="">
                    </div>

                    <div class="col-lg-2 col-md-4 col-6">
                        <img src="assets/img/clients/client-3.png" class="img-fluid" alt="">
                    </div>

                    <div class="col-lg-2 col-md-4 col-6">
                        <img src="assets/img/clients/client-4.png" class="img-fluid" alt="">
                    </div>

                    <div class="col-lg-2 col-md-4 col-6">
                        <img src="assets/img/clients/client-5.png" class="img-fluid" alt="">
                    </div>

                    <div class="col-lg-2 col-md-4 col-6">
                        <img src="assets/img/clients/client-6.png" class="img-fluid" alt="">
                    </div>

                </div>

            </div>
        </section> --}}


        <!-- End Clients Section -->

        <section id="clients" class="clients">
            <div class="container" data-aos="zoom-in">
                <div class="section-title">
                    <span>Support</span>
                    <h2>Support</h2>

                </div>
                <div class="text-center">

                    <p> Melakukan perubahan dalam kehidupan seseorang tidak memerlukan usaha besar,
                    </p>
                    <p>hanya diperlukan
                        keinginan tulus untuk memberikan bantuan.</p>
                    <a class="btn btn-danger" href="#">Donasi</a>
                </div>

            </div>
        </section>
        <br><br><br>

        <!-- ======= Contact Section ======= -->

        {{-- <section id="contact" class="contact">
            <div class="container">

                <div class="section-title">
                    <span>Contact</span>
                    <h2>Contact</h2>
                    <p>Sit sint consectetur velit quisquam cupiditate impedit suscipit alias</p>
                </div>

                <div class="row" data-aos="fade-up">
                    <div class="col-lg-6">
                        <div class="info-box mb-4">
                            <i class="bx bx-map"></i>
                            <h3>Our Address</h3>
                            <p>A108 Adam Street, New York, NY 535022</p>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="info-box  mb-4">
                            <i class="bx bx-envelope"></i>
                            <h3>Email Us</h3>
                            <p>contact@example.com</p>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="info-box  mb-4">
                            <i class="bx bx-phone-call"></i>
                            <h3>Call Us</h3>
                            <p>+1 5589 55488 55</p>
                        </div>
                    </div>

                </div>

                <div class="row" data-aos="fade-up">

                    <div class="col-lg-6 ">
                        <iframe class="mb-4 mb-lg-0"
                            src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621"
                            frameborder="0" style="border:0; width: 100%; height: 384px;" allowfullscreen></iframe>
                    </div>

                    <div class="col-lg-6">
                        <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <input type="text" name="name" class="form-control" id="name"
                                        placeholder="Your Name" required>
                                </div>
                                <div class="col-md-6 form-group mt-3 mt-md-0">
                                    <input type="email" class="form-control" name="email" id="email"
                                        placeholder="Your Email" required>
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <input type="text" class="form-control" name="subject" id="subject"
                                    placeholder="Subject" required>
                            </div>
                            <div class="form-group mt-3">
                                <textarea class="form-control" name="message" rows="5" placeholder="Message" required></textarea>
                            </div>
                            <div class="my-3">
                                <div class="loading">Loading</div>
                                <div class="error-message"></div>
                                <div class="sent-message">Your message has been sent. Thank you!</div>
                            </div>
                            <div class="text-center"><button type="submit">Send Message</button></div>
                        </form>
                    </div>

                </div>

            </div>
        </section> --}}

        <!-- End Contact Section -->

        </main><!-- End #main -->
        <script>
            var myCarousel = document.querySelector('#carouselExampleCaptions')
            var carousel = new bootstrap.Carousel(myCarousel, {
                interval: 5000

            })
        </script>
    @endsection
