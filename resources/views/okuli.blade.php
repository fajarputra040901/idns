@extends('partial.index')
@section('content')
    <main id="main">

        <!-- ======= Breadcrumbs ======= -->
        <section id="breadcrumbs" class="breadcrumbs">
            <div class="container">

                <ol>
                    <li><a href="/home">Beranda</a></li>
                    <li>Okuli</li>
                </ol>
                <h2>Okuli</h2>

            </div>
        </section><!-- End Breadcrumbs -->

        <!-- ======= Portfolio Details Section ======= -->
        <section id="portfolio-details" class="portfolio-details">
            <div class="container">

                <div class="row gy-4">

                    <div class="col-lg-8">
                        <div class="portfolio-details-slider swiper">
                            <div class="swiper-wrapper align-items-center">

                                <div class="swiper-slide">
                                    <img src="{{ asset('assets/img/baner/poster_okuli_1.jpeg') }}" alt="">
                                </div>

                                <div class="swiper-slide">
                                    <img src="{{ asset('assets/img/baner/poster_okuli_2.jpeg') }}" alt="">
                                </div>



                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="portfolio-info">
                            <h3>Informasi Event</h3>
                            <ul>
                                <li><strong>OKULI</strong></li>
                                <li>Optometri Komunitas Universal Literasi Indonesia</li>
                                <li>Sosialisasi Pengabdian Kepada Masyarakat & Didactic Course Optometri
                                    Komuitas</li>
                                <li>4 November 2023</li>
                                <li><strong>Pendaftaran</strong>: <a target="_blank"
                                        href="https://bit.ly/REGISTRASI-SEMINAROKULI-2023">Klik Disini</a></li>
                            </ul>
                        </div>
                        <div class="portfolio-description">
                            <h2>Deskripsi Event</h2>
                            <p style="text-align: justify;">
                                Optometri Komunitas Universal Literasi Indonesia adalah sebuah acara yang bertujuan untuk
                                mengedukasi dan membuka wawasan mengenai peran penting optometri dalam pelayanan masyarakat.
                                Dengan tema utama 'Sosialisasi Pengabdian Kepada Masyarakat & Didactic Course Optometri
                                Komunitas,' acara ini akan menggali beragam aspek, mulai dari pemeriksaan tajam penglihatan
                                hingga manajemen digitalisasi dan pendokumentasian pengabdian kepada masyarakat. Para
                                peserta juga akan mendapatkan pemahaman mendalam tentang peran anak muda dalam komunitas
                                optometri serta peran serta dan dukungan badan usaha dalam penanggulangan bahaya kebutaan di
                                Indonesia. Selain itu, event ini akan membahas isu krusial terkait kesehatan mata, seperti
                                penanggulangan miopia yang semakin merajalela pada anak-anak usia sekolah.
                            </p>
                        </div>
                    </div>

                </div>

            </div>
        </section><!-- End Portfolio Details Section -->

    </main><!-- End #main -->
@endsection
