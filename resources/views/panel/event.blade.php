@extends('partial.panel')
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="d-flex flex-wrap justify-content-between">
                <h4 class="card-title mb-3">E-Commerce Analytics</h4>
            </div>
            <div class="row">
                <div class="col-lg-9">
                    <div class="d-sm-flex justify-content-between">
                        <div class="dropdown">
                            <button class="btn bg-white btn-sm dropdown-toggle btn-icon-text pl-0" type="button"
                                id="dropdownMenuSizeButton4" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                Mon,1 Oct 2019 - Tue,2 Oct 2019
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton4"
                                data-x-placement="top-start">
                                <h6 class="dropdown-header">Mon,17 Oct 2019 - Tue,25 Oct 2019</h6>
                                <a class="dropdown-item" href="#">Tue,18 Oct 2019 - Wed,26 Oct 2019</a>
                                <a class="dropdown-item" href="#">Wed,19 Oct 2019 - Thu,26 Oct 2019</a>
                            </div>
                        </div>
                        <div>
                            <button type="button" class="btn btn-sm btn-light mr-2">Day</button>
                            <button type="button" class="btn btn-sm btn-light mr-2">Week</button>
                            <button type="button" class="btn btn-sm btn-light">Month</button>
                        </div>
                    </div>
                    <div class="chart-container mt-4">
                        <canvas id="ecommerceAnalytic"></canvas>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div>
                        <div class="d-flex justify-content-between mb-3">
                            <div class="text-success font-weight-bold">Inbound</div>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <div class="font-weight-medium">Current</div>
                            <div class="text-muted">38.34M</div>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <div class="font-weight-medium">Average</div>
                            <div class="text-muted">38.34M</div>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <div class="font-weight-medium">Maximum</div>
                            <div class="text-muted">68.14M</div>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <div class="font-weight-medium">60th %</div>
                            <div class="text-muted">168.3GB</div>
                        </div>
                    </div>
                    <hr>
                    <div class="mt-4">
                        <div class="d-flex justify-content-between mb-3">
                            <div class="text-success font-weight-bold">Outbound</div>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <div class="font-weight-medium">Current</div>
                            <div class="text-muted">458.77M</div>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <div class="font-weight-medium">Average</div>
                            <div class="text-muted">1.45K</div>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <div class="font-weight-medium">Maximum</div>
                            <div class="text-muted">15.50K</div>
                        </div>
                        <div class="d-flex justify-content-between">
                            <div class="font-weight-medium">60th %</div>
                            <div class="text-muted">45.5</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
